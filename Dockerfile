FROM python:3.6
COPY . .
ENTRYPOINT ["python", "echo_hello.py"]